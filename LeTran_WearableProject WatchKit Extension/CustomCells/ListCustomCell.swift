//
//  ListCustomCell.swift
//  LeTran_WearableProject WatchKit Extension
//
//  Created by Sunny Le on 9/11/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import Foundation
import WatchKit

class ListCustomCell: NSObject {
    @IBOutlet var lbl_ListName: WKInterfaceLabel!
}
