//
//  ItemCustomCell.swift
//  LeTran_WearableProject WatchKit Extension
//
//  Created by Sunny Le on 9/20/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import WatchKit

class ItemCustomCell: NSObject {

    @IBOutlet var lbl_ItemName: WKInterfaceLabel!
    @IBOutlet var cellGroup: WKInterfaceGroup!
}
