//
//  InterfaceController.swift
//  LeTran_WearableProject WatchKit Extension
//
//  Created by Sunny Le on 9/10/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {
    
    // Outlet
    @IBOutlet var tbw_ListTable: WKInterfaceTable!
    
    // Member variables
    fileprivate let session: WCSession? = WCSession.isSupported() ? WCSession.default : nil
    var everythingList: [List] = []
    var colorHex: String!
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        let sendValues: [String: Any] = ["theList": true]
        let sendColor: [String: Any] = ["getColor": true]

        if let session = self.session, session.isReachable {
            session.sendMessage(sendValues, replyHandler: { (receivedData) in
                DispatchQueue.main.async {
                    if let data = receivedData["savedList"] as? Data {
                        NSKeyedUnarchiver.setClass(List.self, forClassName: "List")
                        if let allLists = NSKeyedUnarchiver.unarchiveObject(with: data) as? [List] {
                            self.everythingList = allLists
                            self.tbw_ListTable.setNumberOfRows(allLists.count, withRowType: "listCell")
                            for index in 0...allLists.count - 1 {
                                guard let cell = self.tbw_ListTable.rowController(at: index) as? ListCustomCell else {
                                    print("Cannot cast cell")
                                    return
                                }
                                cell.lbl_ListName.setText(allLists[index].listName)
                            }
                        }
                    }
                }
            }, errorHandler: nil)
            
            session.sendMessage(sendColor, replyHandler: { (receivedData) in
                DispatchQueue.main.async {
                    if let data = receivedData["theColor"] as? Data {
                        if let color = NSKeyedUnarchiver.unarchiveObject(with: data) as? String {
                            self.colorHex = color
                        }
                    }
                }
            }, errorHandler: nil)
        }
    }
    
    override init() {
        super.init()
        session?.delegate = self
        session?.activate()
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    override func contextForSegue(withIdentifier segueIdentifier: String, in table: WKInterfaceTable, rowIndex: Int) -> Any? {
        let sendToNext = (everythingList, colorHex, rowIndex)
        return sendToNext
    }
    
    

}
