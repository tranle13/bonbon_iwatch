//
//  ItemsInterfaceController.swift
//  LeTran_WearableProject WatchKit Extension
//
//  Created by Sunny Le on 9/20/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class ItemsInterfaceController: WKInterfaceController, WCSessionDelegate {
    
    // Member variables
    fileprivate let session: WCSession? = WCSession.isSupported() ? WCSession.default : nil
    var chosenIndex: Int!
    var chosenList: [(String, Bool)] = []
    var chosenColor: String!
    var wholeList: [List]!
    
    // Outlets
    @IBOutlet var tbw_ItemsTable: WKInterfaceTable!
    
    override init() {
        super.init()
        session?.delegate = self
        session?.activate()
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) { }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        if let passedData = context as? ([List], String?, Int) {
            chosenIndex = passedData.2
            wholeList = passedData.0
            
            if passedData.1 != nil {
                chosenColor = passedData.1
            }
            
            for sector in passedData.0[chosenIndex].listItems {
                for item in sector.value {
                    chosenList.append((item, sector.key))
                }
            }
            
            self.tbw_ItemsTable.setNumberOfRows(chosenList.count, withRowType: "itemCell")
            for index in 0...chosenList.count - 1 {
                guard let cell = self.tbw_ItemsTable.rowController(at: index) as? ItemCustomCell else {
                    print("Cannot cast cell")
                    return
                }
                cell.lbl_ItemName.setText(chosenList[index].0)
                
                if chosenList[index].1, chosenColor != nil {
                    cell.cellGroup.setBackgroundColor(UIColor(hexCode: chosenColor))
                }
            }
        }
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
