# Bon Bon - Lists of Everything
- **Name: Tran Le**

---

## General Functionality Description
Bon bon is an application that will allow user to log in multiple of lists and they can have multiple items for each list. The lists can be grocery lists, chore list or task list and user would have the ability to add and edit everything in the list. When the user is done with one item, they can select it and the system will change the color of the cell background in order to mark that item as completed. The application will also provide user with a tool to change the color of the completed item cell. These heavy features are going to be set up and stored on the phone. The watch would be used to retrieve the data from user's phone and they can scroll through the list, mark items as completed on the watch and all the changes would be updated on the phone as well.

---

## Core Features
- Save lists of items.
- Advanced CRUD operations.
- Color marks for complete items.

---

## Deployment Target 
The application can compile and launch on an iOS 10.x devices and watchOS 3.x devices.






